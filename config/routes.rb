Rails.application.routes.draw do
  get 'test/index'
  get 'home/index'
  namespace :v1 do
    get 'clients/index'
  end

  require 'sidekiq/web'
  require 'sidekiq-scheduler/web'
  
  mount Sidekiq::Web => '/sidekiq'
  Sidekiq::Web.set :session_secret, Rails.application.credentials[:secret_key_base]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'home#index'
end
