require 'sidekiq-scheduler'

class ClientSyncJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # return unless Rails.env.production? # job will only run on production

    sc = SyncClients.new
    sc.execute
  end
end
