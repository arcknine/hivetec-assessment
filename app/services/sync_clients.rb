require 'httparty'
require 'csv'

class SyncClients
  API_URL = "http://localhost:3001/v1/clients.csv"
  
  def initialize
    
  end
  
  def execute
    response = HTTParty.get(API_URL)
    csv = CSV.parse(response.body, :headers => true, :encoding => 'ISO-8859-1')
    
    csv.each do |row|
      c = Client.find_or_initialize_by(email: row["Email"])
      c.first_name = row["First Name"]
      c.last_name = row["Last Name"]
     
      if c.save
        # save residential address
        a1 = c.addresses.find_or_initialize_by(address_type: 'residential')
        a1.street = row["Residential Address Street"]
        a1.locality = row["Residential Address Locality"]
        a1.state = row["Residential Address State"]
        a1.postcode = row["Residential Address Postcode"]
        a1.save
        
        # save postal address
        a2 = c.addresses.find_or_initialize_by(address_type: 'postal')
        a2.street = row["Postal Address Street"]
        a2.locality = row["Postal Address Locality"]
        a2.state = row["Postal Address State"]
        a2.postcode = row["Postal Address Postcode"]
        a2.save
      end
    end

    return true
  end
end