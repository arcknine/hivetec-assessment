require 'httparty'

class GeoNames
  USERNAME = Rails.application.credentials.geoname[:username]

  attr_accessor :api_url
  
  def initialize(client)
    @client = client
    @api_url = "http://api.geonames.org/postalCodeSearch?postalcode=#{@client.postcode}&country=AU&username=#{USERNAME}"
  end

  def get_coordinates
    # return nil only on development cause api is only trial version cannot cater 1000 records from
    return nil, nil if Rails.env.development?

    response = HTTParty.get(@api_url)

    return [nil, nil] if response.code != 200

    localities = response.parsed_response["geonames"]["code"]
    
    result = localities.select { |locality| locality["name"].downcase.strip == @client.locality.downcase.strip }.first
    result = result.nil? ? localities.first : result # it will just return the first data on the list

    return result["lat"], result["lng"]  
  end
end