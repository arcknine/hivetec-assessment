module Validator
  extend ActiveSupport::Concern

  included do
    belongs_to :client

    validates :locality, :state, :postcode, presence: true

    before_save :validate_address
  end

  def validate_address
    towns = Town.where("name LIKE ? AND state_code = ? AND postcode = ?", "%#{self.locality}%", self.state, self.postcode)
   
    if towns.count > 0
      # get geo coordinates
      gn = ::GeoNames.new(self)
      self.latitude, self.longitude = gn.get_coordinates
    else
      self.partially_imported = true
      self.locality = nil
      self.state = nil
      self.postcode = nil
    end
  end
end