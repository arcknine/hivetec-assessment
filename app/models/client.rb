class Client < ApplicationRecord
  has_many :addresses, dependent: :destroy
  
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 
  validates :email, :first_name, :last_name, presence: true

end
