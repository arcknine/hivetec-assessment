# README

## System dependencies

* Ruby version 2.5.0
* Rails version 5.2.3
* Redis

## Running in localhost

1. run bundler `bundle install`
2. create database `rake db:create`
3. migrate table `rake db:migrate`
4. create master.key and use`799659959da69faf5ad3913a5696982d` (required to load credentials)
5. start server `rails s`

## How to Run Scheduled/CRON Job(s)

1. Install Redis (https://redis.io/topics/quickstart)
2. run Redis server `redis-server`
3. run sidekiq `bundle exec sidekiq`

## How to Run the Test Suite

1. prepare test environment tables `rake db:test:prepare`
2. run test `rspec .`