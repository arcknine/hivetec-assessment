Fabricator(:address) do
  address_type "residential"
  street       "8540 Charli Summit"
  locality     "AIRLIE BEACH"
  state        "QLD"
  postcode     4802
  latitude     nil
  longitude    nil
end

Fabricator(:error_address, class_name: :address) do
  address_type "residential"
  street       "8540 Charli Summit"
  locality     "AIRLIE BEACH"
  state        "QLD"
  postcode     12345678
  latitude     nil
  longitude    nil
end