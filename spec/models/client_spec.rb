require 'rails_helper'

describe Client do
  it { should allow_value("email@address.com").for(:email) }
  it { should_not allow_value("email.address.com").for(:email) }
  it { should validate_presence_of :email }
  it { should validate_presence_of :first_name }
  it { should validate_presence_of :last_name }
end