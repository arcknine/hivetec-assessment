require 'rails_helper'

describe Address do
  it { should validate_presence_of :locality }
  it { should validate_presence_of :state }
  it { should validate_presence_of :postcode }

  describe "Address creation/update" do
    before(:each) do
      @client = Fabricate(:client)
      @xml = File.read(Rails.root.join('spec', 'fixtures', 'files', 'postalCodeSearch.xml'))
      username = GeoNames::USERNAME

      stub_request(:get, /api.geonames.org\/postalCodeSearch\?country=AU&postalcode=(.*)&username=#{username}/).
        to_return(status: 200, body: @xml, headers: {"Content-type": "text/xml"})
    end

    it "Should check residential address and return nil if invalid" do
      @address = Fabricate(:error_address, client: @client)

      expect( @address.partially_imported ).to eq true
      expect( @address.locality ).to be_nil
      expect( @address.state ).to be_nil
      expect( @address.postcode ).to be_nil
    end

    it "Should check geo api to get lat and long for the address" do
      @address = Fabricate(:address, client: @client)

      expect( @address.latitude ).not_to be_nil
      expect( @address.longitude ).not_to be_nil
    end
  end
end