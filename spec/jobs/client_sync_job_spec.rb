require 'rails_helper'

describe ClientSyncJob, type: :job do
  include ActiveJob::TestHelper

  before(:each) do
    @csv_file = File.read(Rails.root.join('spec', 'fixtures', 'files', 'client_success_sample.csv'))
    @xml = File.read(Rails.root.join('spec', 'fixtures', 'files', 'postalCodeSearch.xml'))
    username = GeoNames::USERNAME

    stub_request(:get,  SyncClients::API_URL).
      to_return(status: 200, body: @csv_file, headers: {"Content-type": "text/csv"})

    stub_request(:get, /api.geonames.org\/postalCodeSearch\?country=AU&postalcode=(.*)&username=#{username}/).
      to_return(status: 200, body: @xml, headers: {"Content-type": "text/xml"})
  end

  subject(:job) { ClientSyncJob.perform_later() }

  it "should start job" do
    expect { 
      job 
    }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'executes perform' do
    expect(SyncClients).to receive(:new).with(no_args).and_call_original
    
    perform_enqueued_jobs { job }
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
