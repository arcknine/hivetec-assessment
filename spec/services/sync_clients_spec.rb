require 'rails_helper'

describe SyncClients do
  describe "Execute SyncClients" do
    before(:each) do
      @csv_file = File.read(Rails.root.join('spec', 'fixtures', 'files', 'client_success_sample.csv'))
      @xml = File.read(Rails.root.join('spec', 'fixtures', 'files', 'postalCodeSearch.xml'))
      username = GeoNames::USERNAME

      stub_request(:get,  SyncClients::API_URL).
        to_return(status: 200, body: @csv_file, headers: {"Content-type": "text/csv"})

      stub_request(:get, /api.geonames.org\/postalCodeSearch\?country=AU&postalcode=(.*)&username=#{username}/).
        to_return(status: 200, body: @xml, headers: {"Content-type": "text/xml"})
    end

    it "should execute SyncClient and create clients" do
      sc = SyncClients.new
      expect {
        sc.execute
      }.to change{Client.count}.by(1)

      expect(Address.count).to eq 2
    end

    it "should execute SyncClient and update clients" do
      @client = Fabricate(:client)
      
      sc = SyncClients.new
      expect {
        sc.execute
      }.to change{Client.last.first_name}.from("John").to("Darcy")
    end
  end
end