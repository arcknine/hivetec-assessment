# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

Town.delete_all
csv_file = File.read(Rails.root.join('lib', 'seeds', 'au-towns-sample.csv'))
csv = CSV.parse(csv_file, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  t = Town.new
  t.name = row['name']
  t.urban_area = row['urban_area']
  t.state_code = row['state_code']
  t.state = row['state']
  t.postcode = row['postcode']
  t.town_type = row['type']
  t.latitude = row['latitude']
  t.longitude = row['longitude']
  t.elevation = row['elevation']
  t.population = row['population']
  t.median_income = row['median_income']
  t.area_sq_km = row['area_sq_km']
  t.local_government_area = row['local_government_area']
  t.region = row['region']
  t.time_zone = row['time_zone']
  t.save

  # puts "#{t.name} #{t.state_code} #{t.postcode} saved"
end

puts "There are now #{Town.count} rows in the towns table"