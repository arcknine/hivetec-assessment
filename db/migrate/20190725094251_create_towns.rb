class CreateTowns < ActiveRecord::Migration[5.2]
  def change
    create_table :towns do |t|
      t.string :name
      t.string :urban_area
      t.string :state_code
      t.string :state
      t.integer :postcode
      t.string :town_type
      t.float :latitude
      t.float :longitude
      t.integer :elevation
      t.integer :population
      t.integer :median_income
      t.float :area_sq_km
      t.string :local_government_area
      t.string :region
      t.string :time_zone

      t.timestamps
    end

    add_index :towns, [:name, :state_code, :postcode]
  end
end
