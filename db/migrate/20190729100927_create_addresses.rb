class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.references :client, foreign_key: true
      t.string :address_type, default: "residential"
      t.string :street
      t.string :locality
      t.string :state
      t.integer :postcode
      t.float :latitude
      t.float :longitude
      t.boolean :partially_imported, :default => false

      t.timestamps
    end

    add_index :addresses, :address_type
  end
end
