# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_29_100927) do

  create_table "addresses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "client_id"
    t.string "address_type", default: "residential"
    t.string "street"
    t.string "locality"
    t.string "state"
    t.integer "postcode"
    t.float "latitude"
    t.float "longitude"
    t.boolean "partially_imported", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_type"], name: "index_addresses_on_address_type"
    t.index ["client_id"], name: "index_addresses_on_client_id"
  end

  create_table "clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_clients_on_email"
  end

  create_table "towns", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "urban_area"
    t.string "state_code"
    t.string "state"
    t.integer "postcode"
    t.string "town_type"
    t.float "latitude"
    t.float "longitude"
    t.integer "elevation"
    t.integer "population"
    t.integer "median_income"
    t.float "area_sq_km"
    t.string "local_government_area"
    t.string "region"
    t.string "time_zone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "state_code", "postcode"], name: "index_towns_on_name_and_state_code_and_postcode"
  end

  add_foreign_key "addresses", "clients"
end
